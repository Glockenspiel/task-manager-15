package ru.t1.sukhorukova.tm.controller;

import ru.t1.sukhorukova.tm.api.controller.ITaskController;
import ru.t1.sukhorukova.tm.api.service.ITaskService;
import ru.t1.sukhorukova.tm.enumerated.Sort;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.exception.entity.TaskNotFoundException;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");

        System.out.println("Enter task name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter task description");
        final String description = TerminalUtil.nextLine();

        taskService.create(name, description);
    }

    private void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task: tasks) {
            if (task == null) continue;
            System.out.println(index++ + ". " + task.getName() + ": " + task.getDescription());
        }
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = taskService.findAll(sort);
        renderTasks(tasks);
    }

    @Override
    public void showTasksByProjectId() {
        System.out.println("[TASK LIST BY PROJECT ID]");

        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();

        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        renderTasks(tasks);
    }

    private void showTask(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + Status.toName(task.getStatus()));
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK BY ID]");

        System.out.println("Enter task id:");
        final String id = TerminalUtil.nextLine();

        final Task task = taskService.findOneById(id);
        showTask(task);
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");

        System.out.println("Enter task index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        final Task task = taskService.findOneByIndex(index);
        showTask(task);
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");

        System.out.println("Enter task id:");
        final String id = TerminalUtil.nextLine();

        System.out.println("Enter task name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter task description:");
        final String description = TerminalUtil.nextLine();

        taskService.updateById(id, name, description);
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");

        System.out.println("Enter task index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("Enter task name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter task description:");
        final String description = TerminalUtil.nextLine();

        taskService.updateByIndex(index, name, description);
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");

        System.out.println("Enter task id:");
        final String id = TerminalUtil.nextLine();

        taskService.removeById(id);
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");

        System.out.println("Enter task index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        taskService.removeByIndex(index);
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS BY ID]");

        System.out.println("Enter task id:");
        final String id = TerminalUtil.nextLine();

        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();

        final Status status = Status.toStatus(statusValue);
        taskService.changeTaskStatusById(id, status);
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");

        System.out.println("Enter task index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();

        final Status status = Status.toStatus(statusValue);
        taskService.changeTaskStatusByIndex(index, status);
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK BY ID]");

        System.out.println("Enter task id:");
        final String id = TerminalUtil.nextLine();

        taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK BY INDEX]");

        System.out.println("Enter task index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        taskService.changeTaskStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeTaskById() {
        System.out.println("[COMPLETE TASK BY ID]");

        System.out.println("Enter task id:");
        final String id = TerminalUtil.nextLine();

        taskService.changeTaskStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[COMPLETE TASK BY INDEX]");

        System.out.println("Enter task index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        taskService.changeTaskStatusByIndex(index, Status.COMPLETED);
    }

}
