package ru.t1.sukhorukova.tm.exception.field;

public final class NumberIncorrectException extends AbstractFieldExceprion {

    public NumberIncorrectException() {
        super("Error! Index is incorrect...");
    }

    public NumberIncorrectException(final String value, final Throwable couse) {
        super("Error! This value '" + value + "' is incorrect...");
    }

}
