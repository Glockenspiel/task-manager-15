package ru.t1.sukhorukova.tm.api.repository;

import ru.t1.sukhorukova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
